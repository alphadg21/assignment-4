/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.montgomery.mapdemo;


import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.maps.GeoPoint;


public class BasicMapActivity extends FragmentActivity {

	private GoogleMap mMap;
    private UiSettings mUiSettings;
    public static final int  MAP_TYPE_HYBRID = 4;
    private Location current;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_demo);
        setUpMapIfNeeded();
    }
    

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        if (mMap != null) {
            updateMyLocation();
            updateTraffic();
        }
    }    
    
    public void onTrafficToggled(View view) {
        updateTraffic();
    }

    private void updateTraffic() {
        mMap.setTrafficEnabled(true);
    }
    
    public void onMyLocationToggled(View view) {
    	Toast.makeText(getApplicationContext(), "onMyLocationToggled ", Toast.LENGTH_SHORT).show();
    	updateMyLocation();
    }

    private void updateMyLocation() {
    	
        mMap.setMyLocationEnabled(true);
        mUiSettings.setMyLocationButtonEnabled(true);
        Location current = mMap.getMyLocation();
       if(current != null)
        Toast.makeText(getApplicationContext(), "Latitude: "+current.getLatitude()+"\nLongitude: "+current.getLongitude(), Toast.LENGTH_LONG).show();
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                
                mMap.setMyLocationEnabled(true);
                mMap.setMapType(MAP_TYPE_HYBRID);
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        mUiSettings = mMap.getUiSettings();
        mMap.setOnMyLocationChangeListener(new OnMyLocationChangeListener()
        {
			@Override
			public void onMyLocationChange(Location location) {
				// TODO Auto-generated method stub
				TextView myLocationText;
			    myLocationText = (TextView)findViewById(R.id.myLocationText);
			      
			    String latLongString = "No location found";
			    String addressString = "No address found";
			    if(location !=null)
			    {
			    	double lat = location.getLatitude();
			        double lng = location.getLongitude();
			        mMap.clear();
		            mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title("You are Here!"));
		            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat,lng)));
		            mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
			        latLongString = "Lat:" + lat + "\nLong:" + lng;
			        double latitude = location.getLatitude();
			        double longitude = location.getLongitude();
			        Geocoder gc = new Geocoder(getApplicationContext(), Locale.getDefault());

			        if (!Geocoder.isPresent())
			          addressString = "No geocoder available";
			        else {
			          try {
			            List<Address> addresses = gc.getFromLocation(latitude, longitude, 1);
			            StringBuilder sb = new StringBuilder();
			            if (addresses.size() > 0) {
			              Address address = addresses.get(0);

			              for (int i = 0; i < address.getMaxAddressLineIndex(); i++)
			                sb.append(address.getAddressLine(i)).append("\n");

			              sb.append(address.getLocality()).append("\n");
			              sb.append(address.getPostalCode()).append("\n");
			              sb.append(address.getCountryName());
			            }
			            addressString = sb.toString();
			          } catch (IOException e) {
			            Log.d("WHEREAMI", "IO Exception", e);
			          }
			        }
			        
			      }
			    myLocationText.setText("Your Current Position is:\n" +
			    	      latLongString + "\n\n" + addressString);
			    
			}
        	
        }
        		);
    
    }
    
}
